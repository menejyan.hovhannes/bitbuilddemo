import React, { useState } from 'react';
import { Text, TouchableHighlight } from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';


export function MbTooltip() {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <Tooltip
      isVisible={isVisible}
      content={<Text>Check this out!</Text>}
      placement="top"
      onClose={() => setIsVisible(false)}
    >
      <TouchableHighlight
        style={{ width: 200, height: 50, backgroundColor: 'red' }}
        onPress={() => setIsVisible(true)}
      >
        <Text>Press me</Text>
      </TouchableHighlight>
    </Tooltip>
  );
}