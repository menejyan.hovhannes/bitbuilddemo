import { MyRnwAspect } from './my-rnw.aspect';

export type { MyRnwMain } from './my-rnw.main.runtime';
export default MyRnwAspect;
export { MyRnwAspect };
