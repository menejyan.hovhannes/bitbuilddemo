import { Aspect } from '@teambit/harmony';

export const MyRnwAspect = Aspect.create({
  id: 'company.scope/envs/my-rnw',
});
