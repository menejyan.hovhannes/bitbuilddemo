
import { WebpackConfigTransformer, WebpackConfigMutator, WebpackConfigTransformContext } from '@teambit/webpack';
import path from 'path';


/**
 * Transformation to apply for both preview and dev server
 * @param config
 * @param _context
 */
function commonTransformation(config: WebpackConfigMutator, _context: WebpackConfigTransformContext) {

  // Add every react-native package that needs compiling
  const compileNodeModules = [
    'react-native-walkthrough-tooltip',
  ].map(moduleName => path.resolve(`node_modules/${moduleName}`));

  const reactNativePackagesRule = {
    test: /\.(jsx?|tsx?)$/,
    include: [compileNodeModules],
    loader: require.resolve('babel-loader'),
    options: {
      cacheDirectory: false, // Should this be false?
      presets: [require.resolve('@babel/preset-env'), require.resolve('@babel/preset-react')],
      plugins: [require.resolve('@babel/plugin-proposal-class-properties')],
    },
  };

  config.addModuleRule(reactNativePackagesRule);

  return config;
}
/**
 * Transformation for the preview only
 * @param config
 * @param context
 * @returns
 */
export const previewConfigTransformer: WebpackConfigTransformer = (
  config: WebpackConfigMutator,
  context: WebpackConfigTransformContext
) => {
  const newConfig = commonTransformation(config, context);
  return newConfig;
};

/**
 * Transformation for the dev server only
 * @param config
 * @param context
 * @returns
 */
export const devServerConfigTransformer: WebpackConfigTransformer = (
  config: WebpackConfigMutator,
  context: WebpackConfigTransformContext
) => {
  const newConfig = commonTransformation(config, context);
  return newConfig;
};

